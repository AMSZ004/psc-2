const path = require('path');
const inflection = require('inflection');
require('dotenv').config();

console.log(process.env.DB_USERNAME);
module.exports = {
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE_NAME,
  host: process.env.DB_HOST,
  dialect: 'mysql',
  models: [path.join(process.cwd(), 'src/db/models')],
  modelMatch: (_filename, _member) => {
    const filename = inflection.camelize(_filename.replace('.model', ''));
    console.log(filename);

    const member = _member;
    return filename === member;
  },
  timezone: '+09:00',
};
