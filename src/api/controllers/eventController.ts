import asyncHandler from '../../utils/catchAsync';
import { Request, Response, NextFunction } from 'express';
import connection from '../../db/config/config';
import Event from '../../db/models/Event';

const queryParser = require('../../utils/sequelizeQueryParser.js')(connection);

export const createEvent = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    if (!req.file) {
      return res.status(400).json({
        status: 'fail',
        message: 'img is required',
      });
    }
    req.body.img = req.file.filename;
    const event = await Event.create(req.body);
    return res.status(201).json({
      status: 'success',
      payload: event,
    });
  }
);

export const getAllEvents = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    let query = await queryParser.parse(req);
    const events = await Event.findAll(query);

    return res.status(200).json({
      status: 'success',
      results: events!.length,
      payload: events,
    });
  }
);

export const getEvent = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    const event = await Event.findByPk(id);
    if (!event) {
      return res.status(404).json({
        status: 'fail',
        message: 'event not found',
      });
    }

    return res.status(200).json({
      status: 'success',
      payload: event,
    });
  }
);

export const updateEvent = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    if (req.file) req.body.img = req.file.filename;
    const updatedEvent = await Event.update(req.body, {
      where: {
        id,
      },
    });

    if (!updatedEvent) {
      return res.status(404).json({
        status: 'fail',
        message: 'event not found',
      });
    }

    return res.status(200).json({
      status: 'success',
      payload: updatedEvent,
    });
  }
);

export const deleteEvent = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params;
    const deletedUserType = await Event.destroy({ where: { id } });

    if (!deletedUserType) {
      return res.status(404).json({
        status: 'fail',
        message: 'event not found',
      });
    }

    return res.status(200).json({
      status: 'success',
      message: `event with id:${id} deleted successfully`,
    });
  }
);
