import asyncHandler from '../../utils/catchAsync';
import { Request, Response, NextFunction } from 'express';
import { Email } from '../../utils/email';

export const sendEmailDonation = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    await new Email(req.body.fullName, req.body.email).sendDonation();
    return res.status(200).json({
      status: 'success',
      message: 'email is sent',
    });
  }
);

export const sendEmailEvent = asyncHandler(
  async (req: Request, res: Response, next: NextFunction) => {
    await new Email(req.body.fullName, req.body.email).sendEvent();
    return res.status(200).json({
      status: 'success',
      message: 'email is sent',
    });
  }
);
