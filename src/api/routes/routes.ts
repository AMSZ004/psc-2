import express, { Express } from 'express';
import eventsRouter from './events';
import emailsRouter from './email';

const app: Express = express();

app.use('/events', eventsRouter);
app.use('/emails', emailsRouter);

export default app;
