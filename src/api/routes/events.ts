import express from 'express';
import { imageUpload } from '../../utils/multer';

import {
  createEvent,
  getEvent,
  getAllEvents,
  updateEvent,
  deleteEvent,
} from '../controllers/eventController';

const router = express.Router();

router
  .route('/')
  .post(imageUpload.single('img'), createEvent)
  .get(getAllEvents);

router
  .route('/:id')
  .get(getEvent)
  .put(imageUpload.single('img'), updateEvent)
  .delete(deleteEvent);

export default router;
