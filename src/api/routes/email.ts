import express from 'express';

import {
  sendEmailDonation,
  sendEmailEvent,
} from '../controllers/emailController';

const router = express.Router();

router.route('/donation').post(sendEmailDonation);

router.route('/event').post(sendEmailEvent);

export default router;
