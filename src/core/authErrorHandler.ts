import { CustomError } from './customError';

const handleSignatureValidation = () =>
  new CustomError('UnAuthenticated', 401);
const handleDataBaseErrors = () => new CustomError('Internal Error', 500);
const handleJWTExpiredError = () =>
  new CustomError('Your token has expired! Please log in again.', 401);
const handleBadParams = () => new CustomError('Bad Request', 400);
const handleNotFoundError = () => new CustomError('Not Found', 404);
const UnauthorizedError = () =>
  new CustomError('You do not have permission to perform this action', 403);
export {
  handleSignatureValidation,
  handleJWTExpiredError,
  handleDataBaseErrors,
  UnauthorizedError,
  handleBadParams,
  handleNotFoundError
};
