import { environment } from '../../config';
import { Response, Request, NextFunction } from 'express';
import {
  handleBadParams,
  handleDataBaseErrors,
  handleJWTExpiredError,
  handleNotFoundError,
  handleSignatureValidation,
  UnauthorizedError,
} from './authErrorHandler';
import { ApiError, InternalError } from './apiError';

const sendErrorDev = (err: any, res: Response) => {
  res.status(err.statusCode).json({
    status: err.status,
    error: err,
    message: err.message,
    stack: err.stack,
  });
};

const sendErrorProd = (err: any, res: Response) => {
  // Operational, trusted error: send message to client

  if (err.isOperational) {
    res.status(err.statusCode).json({
      status: err.status,
      message: err.message,
    });

    // Programming or other unknown error: don't leak error details
  } else {
    // 1) Log error
    // 2) Send generic message
    res.status(500).json({
      status: 'error',
      message: 'Something went  wrong!',
    });
  }
};

const globalErrorHandler: any = (
  err: any,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  err.statusCode = err.statusCode || 500;
  err.status = err.status || 'error';

  if (environment === 'development') {
    sendErrorDev(err, res);
  } else if (environment === 'production') {
    let error = { ...err };

    if (error.name && err.name.includes('Sequelize'))
      error = handleDataBaseErrors();
    if (error.name === 'TokenExpiredError') error = handleJWTExpiredError();
    if (error.statusCode === 401) error = handleSignatureValidation();
    if (error.statusCode === 403) error = UnauthorizedError();
    if (error.statusCode === 422) error = handleBadParams();
    if (error.statusCode === 404) error = handleNotFoundError();
    sendErrorProd(error, res);
  }
};

export default globalErrorHandler;

export const globalErrorHandlerSecond: any = (
  err: any,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (err.name && err.name.includes('SequelizeForeignKeyConstraintError')) {
    return res.status(400).send({
      status: 'fail',
      message: 'one or many ids provided in the body are invalid',
    });
  } else if (err instanceof ApiError) {
    ApiError.handle(err, res);
  } else {
    if (process.env.NODE_ENV === 'development') {
      return res.status(400).send(err.message);
    }
    ApiError.handle(new InternalError(), res);
  }
};
