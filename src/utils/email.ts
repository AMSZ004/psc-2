import nodemailer from 'nodemailer';
import { convert } from 'html-to-text';
import pug from 'pug';

declare var process: {
  env: {
    NODE_ENV: string;
    SENDINBLUE_HOST: string;
    SENDINBLUE_PORT: string;
    SENDINBLUE_USERNAME: string;
    SENDINBLUE_PASSWORD: string;
  };
};

export class Email {
  to: string;
  fullName: string;
  from: string;
  constructor(fullName: string, email: string) {
    this.to = email;
    this.fullName = fullName;
    this.from = `Donation <donation.softylines@gmail.tn>`;
  }

  newTransport() {
    //Sendinblue
    return nodemailer.createTransport({
      host: process.env.SENDINBLUE_HOST,
      port: 587,
      auth: {
        user: process.env.SENDINBLUE_USERNAME,
        pass: process.env.SENDINBLUE_PASSWORD,
      },
    });
  }

  //send the actual email
  async send(template: any, subject: any, message?: string) {
    // 1) Render HTML based on a pug template
    const html = pug.renderFile(`${__dirname}/../pug/${template}.pug`, {
      fullName: this.fullName,
      message,
      subject,
    });
    // 2) Define email options
    const mailOptions = {
      from: this.from,
      to: this.to,
      subject: subject,
      html,
      text: convert(html),
    };

    // 3) Create a transport and send email
    await this.newTransport().sendMail(mailOptions);
  }

  //Welcome
  async sendDonation() {
    await this.send('donation', 'Thanks for your donation !');
  }
  async sendEvent() {
    await this.send('event', 'Thanks for your donation !');
  }
}
