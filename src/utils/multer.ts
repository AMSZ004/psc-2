import multer from 'multer';

const imageStorage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './public/images');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + file.originalname);
  },
});

export const imageUpload = multer({
  fileFilter: (req: any, file: any, cb: any) => {
    if (file.mimetype.startsWith('image')) {
      cb(null, true);
    } else {
      cb(new Error('plz select an image'), false);
    }
  },
  storage: imageStorage,
});
