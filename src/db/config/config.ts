import { Sequelize } from 'sequelize-typescript';
import { db } from '../../../config';

db.models = [__dirname + '/../models'];

const connection = new Sequelize(db);
export default connection;
