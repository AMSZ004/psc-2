import {
  Table,
  Model,
  Column,
  CreatedAt,
  UpdatedAt,
  DeletedAt,
  DataType,
} from 'sequelize-typescript';

import { Optional } from 'sequelize/types';

interface IEvent {
  id: number;
  title: string;
  description: string;
  place: string;
  img: string;
  date_start: Date;
  date_end: Date;
  created_at: Date;
  updated_at: Date;
  deleted_at: Date;
}

export interface IEventInput
  extends Optional<IEvent, 'id' | 'created_at' | 'updated_at' | 'deleted_at'> {}

@Table({
  timestamps: true,
  tableName: 'event',
})
//
export default class Event extends Model implements IEvent {
  @Column({
    type: DataType.INTEGER,
    primaryKey: true,
    autoIncrement: true,
    allowNull: false,
  })
  declare id: number;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  declare title: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  declare description: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  declare place: string;

  @Column({
    type: DataType.STRING,
  })
  declare img: string;

  @Column({
    type: DataType.DATE,
    allowNull: false,
  })
  declare date_start: Date;

  @Column({
    type: DataType.DATE,
    allowNull: false,
  })
  declare date_end: Date;

  @CreatedAt
  declare created_at: Date;
  @UpdatedAt
  declare updated_at: Date;
  @DeletedAt
  declare deleted_at: Date;
}
