import express, { Express } from 'express';
import morgan from 'morgan';
import cookieParser from 'cookie-parser';
import { globalErrorHandlerSecond } from './src/core/globalErrorHandler';
import RouteV1 from './src/api/routes/routes';
import cors from 'cors';
import helmet from 'helmet';

import hpp from 'hpp';
import { corsUrl } from './config';

const app: Express = express();

app.use(express.static('public'));
app.use(hpp());
app.use(helmet());
const corsOptions = {
  exposedHeaders: 'Authorization',
  credentials: true,
  origin: corsUrl,
  optionsSuccessStatus: 200, // For legacy browser support
};

app.use(cors(corsOptions));
app.use(cookieParser());
app.use(express.json());
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'));
}

app.use('/api', RouteV1);
app.use(globalErrorHandlerSecond);

export default app;
