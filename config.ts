import { Dialect } from 'sequelize/types';
import 'dotenv/config';
import { SequelizeOptions } from 'sequelize-typescript';

export const environment = process.env.NODE_ENV;

export const port = process.env.PORT;

export const corsUrl = process.env.CORS_URL;

export const logDirectory = process.env.LOG_DIR;

export const db: SequelizeOptions = {
  dialect: (process.env.DB_DIALECT as Dialect) || 'mysql',
  host: process.env.DB_HOST || '',
  username: process.env.DB_USERNAME || '',
  password: process.env.DB_PASSWORD || '',
  database: process.env.DB_DATABASE_NAME || '',
  storage: process.env.DB_STORAGE || '',
  logging: false,
};

export const emailProvider = {
  username: process.env.EMAIL_USERNAME,
  password: process.env.EMAIL_PASSWORD,
  host: process.env.EMAIL_HOST,
  port: process.env.EMAIL_PORT,
  sender: process.env.EMAIL_SENDER,
};
