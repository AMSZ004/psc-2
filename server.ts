import 'reflect-metadata';
import app from './app';
import connection from './src/db/config/config';
import 'dotenv/config';
import logger from './src/core/logger';
import { port } from './config';

process.on('uncaughtException', (err) => {
  console.log('UNCAUGHT EXCEPTION! 💥 Shutting down...');
  console.log(err.name, err);
  process.exit(1);
});

const PORT = port || 3002;
const server = app
  .listen(PORT, async () => {
    await connection.sync();
    logger.info(`Server started on port ${PORT}`);
  })
  .on('error', (e) => logger.error(e));

process.on('unhandledRejection', (err: any) => {
  logger.info('UNHANDLED REJECTION! 💥 Shutting down...');
  logger.error(err, err);
  server.close(() => {
    process.exit(1);
  });
});

const signals = ['SIGTERM', 'SIGINT'];
function gracefulShutdown(signal: string) {
  process.on(signal, async () => {
    server.close();
    await connection.close();
    logger.info('My work here is done');
    process.exit(0);
  });
}

signals.forEach((sig: string, i) => gracefulShutdown(signals[i]));
